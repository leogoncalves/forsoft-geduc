﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/layout.Master" AutoEventWireup="true" CodeBehind="cadastro_nota.aspx.cs" Inherits="geducSite.View.cadastro_nota" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Conteudo" runat="server">
<div class="box-content">
    <form id="frmNota" runat="server">       

            <fieldset>
                <legend>Notas</legend>
                
                <div class="form-group">
                    <asp:Label ID="lblDate" runat="server" Text="Data:" class="col-sm-3 control-label"/>
                    <asp:TextBox ID="txtDate" runat="server" required="required"  type="date" class="form-control"/>
                </div>
                
                <div class="form-group">
                    <asp:DropDownList ID="ddlAluno" runat="server"  class="form-control"/>
                </div>
                
                <div class="form-group">
                    <asp:Label ID="lblDisciplina" runat="server" Text="Disciplina:" class="col-sm-3 control-label"/>
                    <asp:TextBox ID="txtDisciplina" runat="server"  class="form-control"/>
                </div>

                <div class="form-group">
                    <asp:DropDownList ID="ddlDisciplina" runat="server"  class="form-control"/>
                </div>
                
                <div class="form-group">
                    <asp:Label ID="lblNota" runat="server" Text="Nota " class="col-sm-3 control-label"/>
                    <asp:TextBox ID="txtNota" runat="server"  class="form-control"/>
                </div>
                
                <div class="form-group">
                    <asp:Label ID="lblPeriodo" runat="server" Text="Periodo:" class="col-sm-3 control-label"/>
                    <asp:TextBox ID="txtPeriodo" runat="server"  class="form-control"/>
                </div>
                
                <div class="form-group">
                    <asp:Label ID="lblOrigem" runat="server" Text="Origem:" class="col-sm-3 control-label"/>
                    <asp:TextBox ID="txtOrigem" runat="server" class="form-control" />
                </div>
                


                <div class="clearfix"></div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-2">
                            <asp:Button ID="btnSalvar" runat="server" class="#" Text="Salvar" />
                        </div>
                        <div class="col-sm-2">
                            <asp:Button ID="btnAdicionarNota" class="#" runat="server" Text="Adicionar Nota" OnClick="btnAdicionarNota_Click" />
                        </div>
                        <div class="col-sm-2">
                            <asp:Button ID="btnExcluirNota" class="#" runat="server" Text="Excluir Nota" />
                        </div>

                        <div class="col-sm-2">
                            <asp:Label ID="lblMensagem" runat="server" />
                        </div>
                    </div>

                
                
                

                

            </fieldset>


        
    </form>
</div>
    
</asp:Content>
