﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using geducSite.Persistence;
using geducSite.Models;

namespace geducSite.View
{
    public partial class cadastro_nota : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                carregarListas();
            }
        }

        private void carregarListas()
        {
            try
            {
                //todos os dois é para poder pegar o id 
                //carregando dropdownlist de aluno
                AlunoDAO ad = new AlunoDAO();

                ddlAluno.DataSource = ad.ListarAluno();
                ddlAluno.DataValueField = "idAluno";
                ddlAluno.DataTextField = "matricula";
                ddlAluno.DataBind();


                //carregando dropdownlist de disciplina
                DisciplinaDAO dDao = new DisciplinaDAO();

                ddlDisciplina.DataSource = dDao.ListarDisciplinas();
                ddlDisciplina.DataValueField = "idDisciplina";
                ddlDisciplina.DataTextField = "nomeD";
                ddlDisciplina.DataBind();

            }
            catch (Exception ex)
            {

                lblMensagem.Text = ex.Message;
            }

 
        }

        protected void btnAdicionarNota_Click(object sender, EventArgs e)
        {
            try
            {
                Nota n = new Nota();
                n.aluno = new Aluno();
                n.disciplina = new Disciplina();

                n.disciplina.idDisciplina = Convert.ToInt32(ddlDisciplina.SelectedValue);
                n.aluno.idAluno = Convert.ToInt32(ddlAluno.SelectedValue);
                n.data = Convert.ToDateTime(txtDate.Text);
                n.nota = float.Parse(txtNota.Text);
                n.periodo = txtPeriodo.Text;
                n.origem = txtOrigem.Text;

                NotaDAO dDAO = new NotaDAO();
                dDAO.CadastrarNota(n);
            }
            catch (Exception ex)
            {

                lblMensagem.Text = ex.Message;
   
            }
        }

    }
}