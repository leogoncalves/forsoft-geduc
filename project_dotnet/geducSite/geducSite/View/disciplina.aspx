﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/layout.Master" AutoEventWireup="true" CodeBehind="disciplina.aspx.cs" Inherits="geducSite.View.disciplina" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Conteudo" runat="server">
    <div class="form-content">
        <form id="form1" runat="server">
            <fieldset>
            <legend>Disciplina</legend>
            <div class="form-group">
                <label for="lblId" class="col-sm-3 control-label">Identifica&ccedil;&atilde;o</label>
                <asp:DropDownList ID="ddlId" class="form-control" runat="server" />
                <asp:Label ID="lvlID" runat="server" CssClass="alert" Text="Campo destinado a busca e altera&ccedil;&atilde;o" />
            </div>
            
            <div class="form-group"></div>
                <label for="lblCodigoDisciplina" class="col-sm-3 control-label">C&oacute;digo da Disciplina: </label>
                <asp:TextBox ID="txtCodigoDisciplina" class="form-control" runat="server"></asp:TextBox>
            </div>
            
            <div class="form-group"></div>
                <label for="lblNomeDisciplina" class="col-sm-3 control-label">Nome da Disciplina: </label>
                <asp:TextBox ID="txtNomeDisciplina" class="form-control" runat="server"></asp:TextBox>
            </div>
            
            <div class="form-group"></div>
                <label for="lblCargaHorariaDisciplina" class="col-sm-3 control-label">Carga Hor&aacute;ria: </label>
                <asp:TextBox ID="txtCargaHorariaDisciplina" class="form-control" runat="server" TextMode="Number"></asp:TextBox>
            </div>
            
            <div class="form-group">
                <span id="msg" runat="server" class="form-control"></span>
            </div>

            <div class="clearfix"></div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-2">
                            <input type="reset" id="btnLimpar" value="Limpar" />
                        </div>
                        <div class="col-sm-2">
                            <asp:Button ID="btnListar" runat="server" Text="Listar" OnClick="btnListar_Click" />
                        </div>
                        <div class="col-sm-2">
                            <asp:Button ID="btnBuscar" runat="server" Text="Buscar" OnClick="btnBuscar_Click" />
                        </div>

                        <div class="col-sm-2">
                            <asp:Button ID="btnAlterar" runat="server" Text="Alterar" OnClick="btnAlterar_Click" />
                        </div>
                        <div class="col-sm-2">
                            <asp:Button ID="btnCadastrar" runat="server" Text="Cadastrar" OnClick="btnCadastrar_Click" />
                        </div>
                        <div class="col-sm-2">
                            <asp:Button ID="btnDeletar" runat="server" Text="Deletar" OnClick="btnDeletar_Click" />
                        </div>
                    </div>
            </fieldset>
            
            <asp:GridView ID="grdDisciplinas" runat="server"></asp:GridView>
        </form>
    </div>
</asp:Content>
