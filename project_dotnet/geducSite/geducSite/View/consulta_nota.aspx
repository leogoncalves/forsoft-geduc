﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/layout.Master" AutoEventWireup="true" CodeBehind="consulta_nota.aspx.cs" Inherits="geducSite.View.consulta_nota" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Conteudo" runat="server">

    <form id="form" runat="server" class="form">
            <h2>Notas</h2>
            <asp:Label ID="lblNome" runat="server" Text="Listar por nome:" class="x10"></asp:Label> 
            <asp:TextBox ID="txtNome" runat="server"></asp:TextBox>
            <asp:Button ID="btnListar" runat="server" Text="Listar" class="btn"/>
            <br /><br />
            <!-- ======================================== -->
            <asp:GridView ID="tblLista" runat="server">
                <Columns>
                    <asp:TemplateField HeaderText="Nome">
                    <ItemTemplate>
                    <%# Eval("nome") %>
                    </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="1º Bimestre">
                    <ItemTemplate>
                    <%# Eval("Nome") %>
                    </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="2º Bimestre">
                    <ItemTemplate>
                    <%# Eval("Preco", "{0:c}") %>
                    </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="3º Bimestre">
                    <ItemTemplate>
                    <%# Eval("DataCadastro") %>
                    </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="4º Bimestre">
                    <ItemTemplate>
                    <a href="/Pages/Detalhes.aspx">Abrir Registro</a>
                    </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
            </asp:GridView>
            <!-- ======================================== -->
            <br /><br />
            <asp:Button ID="btnBuscar" runat="server" Text="Buscar" class="btn" />
            <asp:Button ID="btnAlterar" runat="server" Text="Alterar" class="btn"/>
            <asp:Button ID="btnSalvar" runat="server" Text="Salvar" class="btn"/>
            <asp:Button ID="btnDesativar" runat="server" Text="Dasativar" class="btn"/>
            <asp:Button ID="btnVoltar" runat="server" Text="Voltar" class="btn"/>
        </form>
</asp:Content>
