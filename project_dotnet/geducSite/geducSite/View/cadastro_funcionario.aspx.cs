﻿using geducSite.Models;
using geducSite.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace geducSite.View
{
    public partial class cadastro_funcionario : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void Cadastrar_Click(object sender, EventArgs e)
        {
            try
            {
                Funcionario f = new Funcionario();
                f.nome = txtNomeFuncionario.Text;
                f.dataNascimento = DateTime.Parse(txtDataNascimento.Text);
                f.sexo = rbSexo.Text;
                f.naturalidade = txtNaturalidade.Text;
                f.nacionalidade = txtNacionalidade.Text;
                f.nomePai = txtNomePai.Text;
                f.nomeMae = txtNomeMae.Text;
                f.etnia = ddlEtnia.SelectedValue;
                f.estadoCivil = ddlEstadoCivil.SelectedValue;
                f.nivelEscolaridade = ddlEscolaridade.SelectedValue;
                f.necessidadeEsp = rbNecessidadeSim.Text;
                Cargo c = new Cargo();
                c.cargo = cargo.Text;

                f.cargo = c;

                f.situacao = ddlSituacao.SelectedValue;

                Documento d = new Documento();
                d.cpf = txtCpf.Text;


                d.rg = txtrg.Text;
                d.dataExpedicao = DateTime.Parse(txtExpedido.Text);
                d.orgaoExpedidor = txtExpedido.Text;
                d.numCertidao = txtNumCertificadoNascimento.Text;
                d.livroCertidao = txtLivroCertidaoNascimento.Text;
                d.folhaCertidao = txtFolhaCertidaoNascimento.Text;
                d.dataEmiCertidao = DateTime.Parse(txtDataCertidaoNascimento.Text);
                d.titEleitor = txtTituloEleito.Text;
                d.certReservista = txtCertificadoReservista.Text;

                f.documento = d;

                Endereco en = new Endereco();


                en.longradouro = txtLogradouro.Text;
                en.numero = txtNumero.Text;
                en.complemento = txtComplemento.Text;
                en.bairro = txtBairro.Text;
                en.cep = txtCEP.Text;
                en.uf = ddlUF.SelectedValue;
                en.municipio = txtMunicipio.Text;
                en.zona = txtZona.Text;

                f.endereco = en;

                Contato con = new Contato();

                con.telefoneFixo = txtTelefone.Text;
                con.telefoneCelular = txtCelular.Text;
                con.email = txtEmail.Text;
                con.outros = txtOutros.Text;

                f.contato = con;

                FuncionarioDAO fd = new FuncionarioDAO();
                fd.Salvar(f);

            }

            catch (Exception erro) { erro.ToString(); }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            FuncionarioDAO fd = new FuncionarioDAO();
            grdFuncionario.DataSource = fd.listar();
            grdFuncionario.DataBind();
        }
    }
}