﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace geducSite.Models
{
    public class Cargo
    {
        public int idCargo { get; set; }
        public string cargo { get; set; }
        public string funcao { get; set; }

        public List<Funcionario> funcionario { get; set; }
    }
}
