﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace geducSite.Models
{
    public class Responsavel
    {
        public int idResponsavel { get; set; }
        public string grauParentesco { get; set; }
        public string responsavel { get; set; }
    }
}