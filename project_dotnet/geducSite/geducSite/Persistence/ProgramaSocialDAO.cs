﻿using geducSite.Context;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace geducSite.Persistence
{
    public class ProgramaSocialDAO : Conexao
    {

        public int buscarProgramaSocial(String programa)//Busca o ID do Programa Social para ser utilizado como FK do Aluno
        {
            try
            {
                int idPrograma = 0;

                AbrirConexao();

                cmd = new SqlCommand("SELECT * FROM programasocial WHERE nomePrograma = @v1", con);
                cmd.Parameters.AddWithValue("@v1", programa);
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    idPrograma = Convert.ToInt32(dr["idProgramaSocial"]);
                }

                return idPrograma;
            }
            catch
            {
                throw;
            }
            finally
            {
                FecharConexao();
            }
        }

        public DataTable preencherDropPrograma()
        {
            try
            {
                AbrirConexao();

                string sql = "SELECT nomePrograma FROM programaSocial;";

                SqlDataAdapter sqlad = new SqlDataAdapter(sql, con);
                DataTable dt = new DataTable();

                sqlad.Fill(dt);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                FecharConexao();
            }
        }

    }
}