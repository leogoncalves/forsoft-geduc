﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using geducSite.Context;
using geducSite.Models;
using System.Data.SqlClient;

namespace geducSite.Persistence
{
    public class AlunoDAO : Conexao
    {

        public void salvar(Aluno a)
        {

            try
            {
                int idPrograma = 0;
                int idCurso = 0;

                AbrirConexao();

                //Gravar o Login
                cmd = new SqlCommand("INSERT INTO login (usuario, senha, perfilAcesso) VALUES (@v1, @v2, @v3) SELECT SCOPE_IDENTITY();", con);
                cmd.Parameters.AddWithValue("@v1", a.login.usuario);
                cmd.Parameters.AddWithValue("@v2", a.login.senha);
                cmd.Parameters.AddWithValue("@v3", a.login.perfilAcesso);
                a.login.idLogin = Convert.ToInt32(cmd.ExecuteScalar());

                //Gravar o Documento
                cmd = new SqlCommand("INSERT INTO documento (cpf, rg, dataExpedicao, orgaoExpedidor, numCertidao, livroCertidao, folhaCertidao, dataEmiCertidao, titEleitor, certReservista) VALUES (@v1, @v2, @v3, @v4, @v5, @v6, @v7, @v8, @v9, @v10) SELECT SCOPE_IDENTITY();", con);
                cmd.Parameters.AddWithValue("@v1", a.documento.cpf);
                cmd.Parameters.AddWithValue("@v2", a.documento.rg);
                cmd.Parameters.AddWithValue("@v3", a.documento.dataExpedicao);
                cmd.Parameters.AddWithValue("@v4", a.documento.orgaoExpedidor);
                cmd.Parameters.AddWithValue("@v5", a.documento.numCertidao);
                cmd.Parameters.AddWithValue("@v6", a.documento.livroCertidao);
                cmd.Parameters.AddWithValue("@v7", a.documento.folhaCertidao);
                cmd.Parameters.AddWithValue("@v8", a.documento.dataEmiCertidao);
                cmd.Parameters.AddWithValue("@v9", a.documento.titEleitor);
                cmd.Parameters.AddWithValue("@v10", a.documento.certReservista);
                a.documento.idDocumento = Convert.ToInt32(cmd.ExecuteScalar());

                //Gravar o Endereco
                cmd = new SqlCommand("INSERT INTO endereco (logradouro, numero, complemento, bairro, cidade, uf, cep, municipio, zona) VALUES (@v1, @v2, @v3, @v4, @v5, @v6, @v7, @v8, @v9) SELECT SCOPE_IDENTITY();", con);
                cmd.Parameters.AddWithValue("@v1", a.endereco.longradouro);
                cmd.Parameters.AddWithValue("@v2", a.endereco.numero);
                cmd.Parameters.AddWithValue("@v3", a.endereco.complemento);
                cmd.Parameters.AddWithValue("@v4", a.endereco.bairro);
                cmd.Parameters.AddWithValue("@v5", a.endereco.cidade);
                cmd.Parameters.AddWithValue("@v6", a.endereco.uf);
                cmd.Parameters.AddWithValue("@v7", a.endereco.cep);
                cmd.Parameters.AddWithValue("@v8", a.endereco.municipio);
                cmd.Parameters.AddWithValue("@v9", a.endereco.zona);
                a.endereco.idEndereco = Convert.ToInt32(cmd.ExecuteScalar());

                //Gravar o Contato
                cmd = new SqlCommand("INSERT INTO contato (telefoneFixo, telefoneCelular, email, outros) VALUES (@v1, @v2, @v3, @v4) SELECT SCOPE_IDENTITY();", con);
                cmd.Parameters.AddWithValue("@v1", a.contato.telefoneFixo);
                cmd.Parameters.AddWithValue("@v2", a.contato.telefoneCelular);
                cmd.Parameters.AddWithValue("@v3", a.contato.email);
                cmd.Parameters.AddWithValue("@v4", a.contato.outros);
                a.contato.idContato = Convert.ToInt32(cmd.ExecuteScalar());

                //Busca o programa social
                ProgramaSocialDAO psd = new ProgramaSocialDAO();
                if (!a.programaSocial.nomePrograma.Equals("Nenhum")) { idPrograma = psd.buscarProgramaSocial(a.programaSocial.nomePrograma); }
                
                //busca o Curso
                CursoDAO cd = new CursoDAO();
                if (!a.curso.nomeC.Equals("Nenhum")) { idCurso = cd.buscarCurso(a.curso.nomeC); }
                
                //Pessoa

                //Verifica se há programa social e seleciona a instrução de insert
                string sqlPessoa = "INSERT INTO pessoa (nomeP, dataNascimento, sexo, naturalidade, nacionalidade, nomePai, nomeMae, etnia, estadoCivil, nivelEscolaridade, necessidadeEsp, idLogin, idContato, idEndereco, idDocumento) VALUES (@v1, @v2, @v3, @v4, @v5, @v6, @v7, @v8, @v9, @v10, @v11, @v12, @v13, @v14, @v15) SELECT SCOPE_IDENTITY();";
                string sqlPessoaPrograma = "INSERT INTO pessoa (nomeP, dataNascimento, sexo, naturalidade, nacionalidade, nomePai, nomeMae, etnia, estadoCivil, nivelEscolaridade, necessidadeEsp, idLogin, idContato, idEndereco, idDocumento, idProgramaSocial) VALUES (@v1, @v2, @v3, @v4, @v5, @v6, @v7, @v8, @v9, @v10, @v11, @v12, @v13, @v14, @v15, @v16) SELECT SCOPE_IDENTITY();";

                if (idPrograma.Equals(0))
                {
                    cmd = new SqlCommand(sqlPessoa, con);
                    cmd.Parameters.AddWithValue("@v1", a.nome);
                    cmd.Parameters.AddWithValue("@v2", a.dataNascimento);
                    cmd.Parameters.AddWithValue("@v3", a.sexo);
                    cmd.Parameters.AddWithValue("@v4", a.naturalidade);
                    cmd.Parameters.AddWithValue("@v5", a.nacionalidade);
                    cmd.Parameters.AddWithValue("@v6", a.nomePai);
                    cmd.Parameters.AddWithValue("@v7", a.nomeMae);
                    cmd.Parameters.AddWithValue("@v8", a.etnia);
                    cmd.Parameters.AddWithValue("@v9", a.estadoCivil);
                    cmd.Parameters.AddWithValue("@v10", a.nivelEscolaridade);
                    cmd.Parameters.AddWithValue("@v11", a.necessidadeEsp);
                    cmd.Parameters.AddWithValue("@v12", a.login.idLogin);
                    cmd.Parameters.AddWithValue("@v13", a.contato.idContato);
                    cmd.Parameters.AddWithValue("@v14", a.endereco.idEndereco);
                    cmd.Parameters.AddWithValue("@v15", a.documento.idDocumento);
                    a.idPessoa = Convert.ToInt32(cmd.ExecuteScalar());
                }

                if (!idPrograma.Equals(0))
                {
                    cmd = new SqlCommand(sqlPessoaPrograma, con);
                    cmd.Parameters.AddWithValue("@v1", a.nome);
                    cmd.Parameters.AddWithValue("@v2", a.dataNascimento);
                    cmd.Parameters.AddWithValue("@v3", a.sexo);
                    cmd.Parameters.AddWithValue("@v4", a.naturalidade);
                    cmd.Parameters.AddWithValue("@v5", a.nacionalidade);
                    cmd.Parameters.AddWithValue("@v6", a.nomePai);
                    cmd.Parameters.AddWithValue("@v7", a.nomeMae);
                    cmd.Parameters.AddWithValue("@v8", a.etnia);
                    cmd.Parameters.AddWithValue("@v9", a.estadoCivil);
                    cmd.Parameters.AddWithValue("@v10", a.nivelEscolaridade);
                    cmd.Parameters.AddWithValue("@v11", a.necessidadeEsp);
                    cmd.Parameters.AddWithValue("@v12", a.login.idLogin);
                    cmd.Parameters.AddWithValue("@v13", a.contato.idContato);
                    cmd.Parameters.AddWithValue("@v14", a.endereco.idEndereco);
                    cmd.Parameters.AddWithValue("@v15", a.documento.idDocumento);
                    cmd.Parameters.AddWithValue("@v16", idPrograma);
                    a.idPessoa = Convert.ToInt32(cmd.ExecuteScalar());
                }
                

                //Grava o Aluno verificando as FK.
                string sqlAluno = "INSERT INTO aluno (situacao, idPessoa, matricula) VALUES (@v1, @v2, @v3);";
                string sqlAlunoCurso = "INSERT INTO aluno (situacao, idPessoa, matricula, idCurso) VALUES (@v1, @v2, @v3, @v4);";

                if (idCurso != 0)
                {
                    cmd = new SqlCommand(sqlAlunoCurso, con);
                    cmd.Parameters.AddWithValue("@v1", a.situacao);
                    cmd.Parameters.AddWithValue("@v2", a.idPessoa);
                    cmd.Parameters.AddWithValue("@v3", a.matricula);
                    cmd.Parameters.AddWithValue("@v4", idCurso);
                    cmd.ExecuteNonQuery();
                }

                if (idCurso == 0)
                {
                    cmd = new SqlCommand(sqlAluno, con);
                    cmd.Parameters.AddWithValue("@v1", a.situacao);
                    cmd.Parameters.AddWithValue("@v2", a.idPessoa);
                    cmd.Parameters.AddWithValue("@v3", a.matricula);
                    cmd.ExecuteNonQuery();
                }

            }
            catch
            {
                throw;
            }
            finally
            {
                FecharConexao();
            }
        }

        public Aluno buscarAlunoMatricula(string matricula)
        {
            try
            {
                AbrirConexao();

                Aluno a = new Aluno();

                cmd = new SqlCommand(
                        "SELECT * " +
                        "FROM aluno a INNER JOIN pessoa p " +
                        "ON a.idPessoa = p.idPessoa " +
                        "INNER JOIN login l " +
                        "ON l.idLogin = p.idLogin " +
                        "INNER JOIN documento d " +
                        "ON d.idDocumento = p.idDocumento " +
                        "INNER JOIN contato c " +
                        "ON c.idContato = p.idContato " +
                        "INNER JOIN endereco e " +
                        "ON e.idEndereco = p.idEndereco " +
                        "INNER JOIN curso cu " +
                        "ON cu.idCurso = a.idCurso " +
                        "INNER JOIN programaSocial ps " +
                        "ON ps.idProgramaSocial = p.idProgramaSocial " +
                        "WHERE a.matricula = @v1;", con);
                cmd.Parameters.AddWithValue("@v1", matricula);
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {

                    a.login = new Login();
                    a.documento = new Documento();
                    a.contato = new Contato();
                    a.endereco = new Endereco();
                    a.curso = new Curso();
                    a.programaSocial = new ProgramaSocial();


                    a.idAluno = Convert.ToInt32(dr["idAluno"]);
                    a.situacao = Convert.ToString(dr["situacao"]);
                    a.matricula = Convert.ToString(dr["matricula"]);
                    a.login.usuario = Convert.ToString(dr["usuario"]);
                    a.login.senha = Convert.ToString(dr["senha"]);
                    a.nome = Convert.ToString(dr["nome"]);
                    a.dataNascimento = Convert.ToDateTime(dr["dataNascimento"]);
                    a.sexo = Convert.ToString(dr["sexo"]);
                    a.naturalidade = Convert.ToString(dr["naturalidade"]);
                    a.nacionalidade = Convert.ToString(dr["nacionalidade"]);
                    a.nomePai = Convert.ToString(dr["nomePai"]);
                    a.etnia = Convert.ToString(dr["etnia"]);
                    a.estadoCivil = Convert.ToString(dr["estadoCivil"]);
                    a.nivelEscolaridade = Convert.ToString(dr["nivelEscolaridade"]);
                    a.necessidadeEsp = Convert.ToString(dr["necessidadeEsp"]);
                    a.documento.cpf = Convert.ToString(dr["cpf"]);
                    a.documento.rg = Convert.ToString(dr["rg"]);
                    a.documento.dataExpedicao = Convert.ToDateTime(dr["dataExpedicao"]);
                    a.documento.orgaoExpedidor = Convert.ToString(dr["orgaoExpedidor"]);
                    a.documento.numCertidao = Convert.ToString(dr["numCertidao"]);
                    a.documento.livroCertidao = Convert.ToString(dr["livroCertidao"]);
                    a.documento.folhaCertidao = Convert.ToString(dr["folhaCertidao"]);
                    a.documento.dataEmiCertidao = Convert.ToDateTime(dr["dataEmiCertidao"]);
                    a.documento.titEleitor = Convert.ToString(dr["titEleitor"]);
                    a.documento.certReservista = Convert.ToString(dr["certReservista"]);
                    a.contato.telefoneFixo = Convert.ToString(dr["telefoneFixo"]);
                    a.contato.telefoneCelular = Convert.ToString(dr["telefoneCelular"]);
                    a.contato.email = Convert.ToString(dr["email"]);
                    a.contato.outros = Convert.ToString(dr["outros"]);
                    a.endereco.longradouro = Convert.ToString(dr["logradouro"]);
                    a.endereco.numero = Convert.ToString(dr["numero"]);
                    a.endereco.complemento = Convert.ToString(dr["complemento"]);
                    a.endereco.bairro = Convert.ToString(dr["bairro"]);
                    a.endereco.cidade = Convert.ToString(dr["cidade"]);
                    a.endereco.uf = Convert.ToString(dr["uf"]);
                    a.endereco.cep = Convert.ToString(dr["cep"]);
                    a.endereco.municipio = Convert.ToString(dr["municipio"]);
                    a.endereco.zona = Convert.ToString(dr["zona"]);
                    a.curso.nomeC = Convert.ToString(dr["nomeC"]);
                    a.curso.descricao = Convert.ToString(dr["descricao"]);
                    a.curso.cargaHoraria = Convert.ToInt32(dr["cargaHoraria"]);
                    a.curso.codigoC = Convert.ToString(dr["codigo"]);
                    a.curso.codigoC = Convert.ToString(dr["codigo"]);
                    a.curso.modalidadeDeEnsino = Convert.ToString(dr["modalidadeDeEnsino"]);
                    a.curso.nivelEnsino = Convert.ToString(dr["nivelEnsino"]);

                    if (!dr["idProgramaSocial"].Equals(null) || dr["idProgramaSocial"] != DBNull.Value)
                    {
                        a.programaSocial.nomePrograma = Convert.ToString(dr["nomePrograma"]);
                        a.programaSocial.descricao = Convert.ToString(dr["descricao"]);
                        a.programaSocial.ambitoAdm = Convert.ToString(dr["ambitoAdm"]);
                    }
                    

                }

                return a;
            }
            catch
            {
                throw;
            }
            finally
            {
                FecharConexao();
            }
        }

        public Aluno buscarAlunoCpf(string cpf)
        {
            try
            {
                AbrirConexao();

                Aluno a = new Aluno();

                cmd = new SqlCommand(
                        "SELECT * " +
                        "FROM aluno a INNER JOIN pessoa p " +
                        "ON a.idPessoa = p.idPessoa " +
                        "INNER JOIN login l " +
                        "ON l.idLogin = p.idLogin " +
                        "INNER JOIN documento d " +
                        "ON d.idDocumento = p.idDocumento " +
                        "INNER JOIN contato c " +
                        "ON c.idContato = p.idContato " +
                        "INNER JOIN endereco e " +
                        "ON e.idEndereco = p.idEndereco " +
                        "INNER JOIN curso cu " +
                        "ON cu.idCurso = a.idCurso " +
                        "INNER JOIN programaSocial ps " +
                        "ON ps.idProgramaSocial = p.idProgramaSocial " +
                        "WHERE d.cpf = @v1;", con);
                cmd.Parameters.AddWithValue("@v1", cpf);
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {

                    a.login = new Login();
                    a.documento = new Documento();
                    a.contato = new Contato();
                    a.endereco = new Endereco();
                    a.curso = new Curso();
                    a.programaSocial = new ProgramaSocial();


                    a.idAluno = Convert.ToInt32(dr["idAluno"]);
                    a.situacao = Convert.ToString(dr["situacao"]);
                    a.matricula = Convert.ToString(dr["matricula"]);
                    a.login.usuario = Convert.ToString(dr["usuario"]);
                    a.login.senha = Convert.ToString(dr["senha"]);
                    a.nome = Convert.ToString(dr["nome"]);
                    a.dataNascimento = Convert.ToDateTime(dr["dataNascimento"]);
                    a.sexo = Convert.ToString(dr["sexo"]);
                    a.naturalidade = Convert.ToString(dr["naturalidade"]);
                    a.nacionalidade = Convert.ToString(dr["nacionalidade"]);
                    a.nomePai = Convert.ToString(dr["nomePai"]);
                    a.etnia = Convert.ToString(dr["etnia"]);
                    a.estadoCivil = Convert.ToString(dr["estadoCivil"]);
                    a.nivelEscolaridade = Convert.ToString(dr["nivelEscolaridade"]);
                    a.necessidadeEsp = Convert.ToString(dr["necessidadeEsp"]);
                    a.documento.cpf = Convert.ToString(dr["cpf"]);
                    a.documento.rg = Convert.ToString(dr["rg"]);
                    a.documento.dataExpedicao = Convert.ToDateTime(dr["dataExpedicao"]);
                    a.documento.orgaoExpedidor = Convert.ToString(dr["orgaoExpedidor"]);
                    a.documento.numCertidao = Convert.ToString(dr["numCertidao"]);
                    a.documento.livroCertidao = Convert.ToString(dr["livroCertidao"]);
                    a.documento.folhaCertidao = Convert.ToString(dr["folhaCertidao"]);
                    a.documento.dataEmiCertidao = Convert.ToDateTime(dr["dataEmiCertidao"]);
                    a.documento.titEleitor = Convert.ToString(dr["titEleitor"]);
                    a.documento.certReservista = Convert.ToString(dr["certReservista"]);
                    a.contato.telefoneFixo = Convert.ToString(dr["telefoneFixo"]);
                    a.contato.telefoneCelular = Convert.ToString(dr["telefoneCelular"]);
                    a.contato.email = Convert.ToString(dr["email"]);
                    a.contato.outros = Convert.ToString(dr["outros"]);
                    a.endereco.longradouro = Convert.ToString(dr["logradouro"]);
                    a.endereco.numero = Convert.ToString(dr["numero"]);
                    a.endereco.complemento = Convert.ToString(dr["complemento"]);
                    a.endereco.bairro = Convert.ToString(dr["bairro"]);
                    a.endereco.cidade = Convert.ToString(dr["cidade"]);
                    a.endereco.uf = Convert.ToString(dr["uf"]);
                    a.endereco.cep = Convert.ToString(dr["cep"]);
                    a.endereco.municipio = Convert.ToString(dr["municipio"]);
                    a.endereco.zona = Convert.ToString(dr["zona"]);
                    a.curso.nomeC = Convert.ToString(dr["nomeC"]);
                    a.curso.descricao = Convert.ToString(dr["descricao"]);
                    a.curso.cargaHoraria = Convert.ToInt32(dr["cargaHoraria"]);
                    a.curso.codigoC = Convert.ToString(dr["codigo"]);
                    a.curso.codigoC = Convert.ToString(dr["codigo"]);
                    a.curso.modalidadeDeEnsino = Convert.ToString(dr["modalidadeDeEnsino"]);
                    a.curso.nivelEnsino = Convert.ToString(dr["nivelEnsino"]);

                    if (!dr["idProgramaSocial"].Equals(null) || dr["idProgramaSocial"] != DBNull.Value)
                    {
                        a.programaSocial.nomePrograma = Convert.ToString(dr["nomePrograma"]);
                        a.programaSocial.descricao = Convert.ToString(dr["descricao"]);
                        a.programaSocial.ambitoAdm = Convert.ToString(dr["ambitoAdm"]);
                    }

                }

                return a;
            }
            catch
            {
                throw;
            }
            finally
            {
                FecharConexao();
            }
        }

        public List<Aluno> ListarAluno()
        {
            try
            {
                AbrirConexao();
                cmd = new SqlCommand("SELECT * FROM aluno ORDER BY idAluno", con);
                dr = cmd.ExecuteReader();

                List<Aluno> lista = new List<Aluno>();

                while (dr.Read())
                {

                    Aluno a = new Aluno();
                    a.idAluno = Convert.ToInt32(dr["idAluno"]);
                    a.matricula = Convert.ToString(dr["matricula"]);

                    lista.Add(a);
                }
                return lista;
            }
            catch
            {

                throw;
            }
            finally
            {
                FecharConexao();
            }
        }

    }
}