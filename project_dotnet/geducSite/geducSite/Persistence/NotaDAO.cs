﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using geducSite.Models;
using geducSite.Context;
using System.Data.SqlClient;

namespace geducSite.Persistence
{
    public class NotaDAO : Conexao
    {
        public void CadastrarNota(Nota n)
        {
            try
            {
                AbrirConexao();
               
                cmd = new SqlCommand("INSERT INTO nota_aluno (idAluno, idDisciplina, nota, periodo, origem, data) VALUES (@V1, @V2, @V3, @V4, @V5, @V6)", con);
                cmd.Parameters.AddWithValue("@v1", n.aluno.idAluno);
                cmd.Parameters.AddWithValue("@v2", n.disciplina.idDisciplina);
                cmd.Parameters.AddWithValue("@v3", n.nota);
                cmd.Parameters.AddWithValue("@v4", n.periodo);
                cmd.Parameters.AddWithValue("@v5", n.origem);
                cmd.Parameters.AddWithValue("@v6", n.data);
                cmd.ExecuteNonQuery();  
                
                

            }
            catch
            {

                throw;
            }
            finally
            {
                FecharConexao();
            }
        }

    }
}