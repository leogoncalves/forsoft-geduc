﻿using geducSite.Context;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace geducSite.Persistence
{
    public class CursoDAO : Conexao
    {

        public int buscarCurso(String curso) //Busca o ID do Curso para ser utilizado como FK no Aluno
        {
            try
            {
                int idCurso = 0;

                AbrirConexao();

                cmd = new SqlCommand("SELECT * FROM curso WHERE nomeC = @v1", con);
                cmd.Parameters.AddWithValue("@v1", curso);
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    idCurso = Convert.ToInt32(dr["idCurso"]);
                }

                return idCurso;
            }
            catch
            {
                throw;
            }
            finally
            {
                FecharConexao();
            }
        }

        public DataTable preencherDropCurso()
        {
            try
            {
                AbrirConexao();

                string sql = "SELECT nomeC FROM curso;";

                SqlDataAdapter sqlad = new SqlDataAdapter(sql, con);
                DataTable dt = new DataTable();

                sqlad.Fill(dt);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                FecharConexao();
            }
        }

    }
}